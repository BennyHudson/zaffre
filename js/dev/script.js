$(document).ready(function() {

    headerHeight = $('header').outerHeight();

	$('.container').fitVids();
    $('.parallax-window').parallax({
        'bleed': 10,
        'naturalWidth': 1600,
        'naturalHeight': 700
    }); 

    // Lazy Load Images

    $('img.unveil').unveil(200, function() {
        $(this).load(function() {
            this.style.opacity = 1;
        });
    });

    // Scrolling Nav

    $('.page-scroller').click(function(e) {
        e.preventDefault();

        var target = $(this).data('target');

        $('html, body').animate({
            scrollTop: $('#' + target).offset().top - headerHeight
        }, 1000);
    });

    // Isotope

    var $grid = $('.isotope-list').isotope({
      // options
      itemSelector: '.isotope-item',
      layoutMode: 'masonry',
      masonry: {
        columnWidth: '.grid-item'
      }
    });

    $grid.imagesLoaded().progress( function() {
	   $grid.isotope('layout');
	});

});

$(window).scroll(function() {

    var scrollTop = $(this).scrollTop();
    headerHeight = $('header').outerHeight();

    $('.main-section').each(function() {

        var topDistance = $(this).offset().top;

        if ( (topDistance - 85) < scrollTop ) {
            var activeSection = $(this).attr('id');
            
            $('nav.main a.active').removeClass('active')
            $('.target-' + activeSection).addClass('active');  
        }
    });

    if ($(this).scrollTop() > headerHeight) { // this refers to window
        $('header').addClass('fixed');
    } else {
        $('header').removeClass('fixed');
    }

});

enquire
    .register("screen and (min-width:95em)", function() { 
        $(document).ready(function() {

            // Team Slider

            $('.team-slider').imagesLoaded(function() {
                $('.team-slider').bxSlider({
                    slideWidth: 1250,
                    minSlides: 1,
                    maxSlides: 1,
                    moveSlides: 1,
                    pager: false,
                    infiniteLoop: false,
                    adaptiveHeight: true,
                    startSlide: 1,
                    hideControlOnEnd: true,
                    nextText: '<i class="fa fa-chevron-right"></i>',
                    prevText: '<i class="fa fa-chevron-left"></i>'
                }); 
            });

        });
    }, true)
    .register("screen and (max-width:95em)", function() { 
        $(document).ready(function() {
            
            // Team Slider

            $('.team-slider').imagesLoaded(function() {
                $('.team-slider').bxSlider({
                    slideWidth: 1000,
                    minSlides: 1,
                    maxSlides: 1,
                    moveSlides: 1,
                    pager: false,
                    infiniteLoop: false,
                    adaptiveHeight: true,
                    startSlide: 1,
                    hideControlOnEnd: true,
                    nextText: '<i class="fa fa-chevron-right"></i>',
                    prevText: '<i class="fa fa-chevron-left"></i>'
                });
            });

        });
    })
    .register("screen and (max-width:65em)", function() {
        $(document).ready(function() {
            
            // Team Slider

            $('.team-slider').imagesLoaded(function() {
                $('.team-slider').bxSlider({
                    slideWidth: 900,
                    minSlides: 1,
                    maxSlides: 1,
                    moveSlides: 1,
                    infiniteLoop: false,
                    adaptiveHeight: true,
                    startSlide: 1,
                    pager: false,
                    hideControlOnEnd: true,
                    nextText: '<i class="fa fa-chevron-right"></i>',
                    prevText: '<i class="fa fa-chevron-left"></i>'
                });
            });

        });
    })
    .register("screen and (max-width:65em) and (orientation: portrait)", function() {
        $(document).ready(function() {
            
            // Team Slider

            $('.team-slider').imagesLoaded(function() {
                $('.team-slider').bxSlider({
                    slideWidth: 900,
                    minSlides: 1,
                    maxSlides: 1,
                    moveSlides: 1,
                    infiniteLoop: false,
                    adaptiveHeight: true,
                    startSlide: 1,
                    hideControlOnEnd: true,
                    nextText: '<i class="fa fa-chevron-right"></i>',
                    prevText: '<i class="fa fa-chevron-left"></i>'
                });

            });
        });
    })
    .register("screen and (max-width:31em)", function() {
        $(document).ready(function() {
            
            // Team Slider

            $('.flex-list.large').bxSlider({
                controls: false
            });

        });
    })

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
